class CreateLists < ActiveRecord::Migration[5.2]
  def change
    create_table :lists do |t|
      t.string :name
      t.float :cost_per_day
      t.string :trans
      t.date :start_d
      t.date :end_d
      t.integer :user_id

      t.timestamps
    end
  end
end
