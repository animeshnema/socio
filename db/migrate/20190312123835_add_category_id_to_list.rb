class AddCategoryIdToList < ActiveRecord::Migration[5.2]
  def change
    add_column :lists, :categorymain_id, :integer
    add_reference :lists, :category, foreign_key: true
  end
end
