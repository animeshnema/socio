class RemoveItemIdFromAccept < ActiveRecord::Migration[5.2]
  def change
    remove_column :accepts, :item_id, :integer
    remove_column :accepts, :list, :reference
  end
end
