class CreateAccepts < ActiveRecord::Migration[5.2]
  def change
    create_table :accepts do |t|
      t.date :start_d
      t.date :end_d
      t.integer :deal_id

      t.timestamps
    end
  end
end
