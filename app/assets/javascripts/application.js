// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//= require jquery3
//= require bootstrap-datepicker
//= require jquery_ujs
//= require activestorage
//= require turbolinks
//= require_tree .

    
    // get references to select list and display text box
    $(document).ready(function(){
    $('#list_trans').on('click',function(){
        //var optionValue = $(this).val();
        //var optionText = $('#dropdownList option[value="'+optionValue+'"]').text();
        var optionText = $("#list_trans option:selected").text();
        	 if(optionText =="Borrow"){
                $("#list_cost_per_day").val(0);
                $("#list_cost_per_day").prop("disabled", true);
                //$('#list_cost_per_day').prop("disabled", true);
    }
    else
    	 if(optionText =="Lend"){
    	 	    $("#list_cost_per_day").val("");
                $("#list_cost_per_day").prop("disabled", false);
            }
});
});


