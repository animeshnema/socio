class ListsController < ApplicationController
		def index
    @lists = List.all
  end
def new
  @list = List.new
end
 def show
    @list = List.find(params[:id])
   @category=Category.find(@list.categorymain_id)
  end

def create
  @list = List.new(list_params)
   @list.save
    redirect_to @list
  
end
 def destroy
    @list = List.find(params[:id])
    @list.destroy
 
    redirect_to lists_path
  end
private
  def list_params
    params.require(:list).permit(:name,:categorymain_id,:cost_per_day,:trans,:start_d,:end_d)
  end
end

  