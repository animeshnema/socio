class AcceptsController < ApplicationController
	def index
   @list=List.find(params[:list_id])
   @accept =@list.accept.all
  end
def new

   @list=List.find(params[:list_id])
   @accept =@list.accept.new
 		
end
 def show
 	@list=List.find(params[:list_id])
   @accept =@list.accept.find(params[:id])
  end
def create
	@list=List.find(params[:list_id])
  	@accept = @list.accept.new(accept_params)
   if @accept.save
    redirect_to  list_accept_path(params[:list_id],@accept)
  else
    render 'new'
end
end

private
  def accept_params
    params.require(:accept).permit(:list_id,:start_d,:end_d)
  end
end

