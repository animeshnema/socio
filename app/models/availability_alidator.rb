class AvailabilityValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    accepts = Accept.where(["id =?", accept.id])
    date_ranges = accepts.map { |b| b.start_d..b.end_d }

    date_ranges.each do |range|
      if range.include? value
        record.errors.add(attribute, "not available")
      end
    end
  end
end