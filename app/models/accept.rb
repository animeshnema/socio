class Accept < ApplicationRecord
  belongs_to :list
  validates :start_d, :end_d, presence: true
  validate :end_date_after_start_date
  validate :date_need_to_be_inrange
  validate :avaliblity
  private

  def end_date_after_start_date
    return if end_d.blank? || start_d.blank?

    if end_d < start_d
      errors.add(:end_d, "must be after the start date")
    end
 end
 def date_need_to_be_inrange
 	 return if end_d.blank? || start_d.blank?
     if end_d>list.end_d || start_d<list.start_d
     	errors.add(list.start_d.to_s,"start date of the item")
     	errors.add(list.end_d.to_s,"end  date of the item")
    end
end
def avaliblity
	return if end_d.blank? || start_d.blank?
	a=list.accept.pluck(:start_d,:end_d)
	i=0
	while(i!=a.length) do
		 if start_d.between?(a[i][0],a[i][1]) || end_d.between?(a[i][0],a[i][1])
		 	errors.add(a.to_s,"already booked dates")
		 	break
		 end
		end
	end
	
end



