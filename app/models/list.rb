class List < ApplicationRecord
	has_many :accept
	validates :start_d, :end_d, presence: true
end
